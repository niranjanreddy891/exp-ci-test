ERE_INFRA_LOC=http://exp.vlabs.ac.in/ere/ere-develop.tar.gz
INFRA=~/tmp/experiments

CATALOG=ere-infra
THEME=gml


CATALOGS_DIR=${INFRA}/catalogs
CATALOG_REPO=https://gitlab.com/vlead-projects/infra/catalogs/${CATALOG}.git



CATALOG_BRANCH=develop

.ONESHELL:
create-config:
	cd ${CATALOGS_DIR}/${CATALOG}
	rsync config.mk.sample config.mk

install-catalog:
	mkdir -p ${CATALOGS_DIR}
	cd ${CATALOGS_DIR}
	if [ -d ${CATALOG} ]; then
		echo "${CATALOG} is already present"
		(cd ${CATALOG}; git checkout ${CATALOG_BRANCH}; git pull origin ${CATALOG_BRANCH})
	else
		git clone -b ${CATALOG_BRANCH} ${CATALOG_REPO}
	fi

clean-catalog:
	rm -rf ${CATALOGS_DIR}/${CATALOG}/

install-infra: install-catalog create-config
	(cd ${CATALOGS_DIR}/${CATALOG}; make -k install-ere)

clean-infra:
	(cd ${CATALOGS_DIR}/${CATALOG}; make -k clean-ere)
